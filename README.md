#  TobiiController
An api made with Unity to control the tobii glasses. This software allows to manage the creation of projects, users and trials. It also to control the recording and to add events.
    
# Gaze Activity Analysis
Multiple python code use first to extract data from tobbi glasses and then perform analysis on gaze activity, especially on fixations