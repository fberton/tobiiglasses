"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import optparse
import os
from Sources.General.FilesAndDirectory import CreateDirectory


def GetParserFixations():
    parser = optparse.OptionParser("usage: %prog [options] arg1 arg2")
    parser.add_option("-i", "--intput", dest="input",
                      type="string", help="path to the user folder with all the data (recursive search for gaze data)")
    parser.add_option("-m", "--methods", dest="methods",
                      choices=["Centroid2D","Velocity2D","AngleDistance3D"],
                      default = "Centroid2D",
                      help="methods used to compute fixations (Centroid2D,Velocity2D,AngleDistance3D")

    return parser.parse_args()

# Recursive function to compute fixations in each folder
def ProcessComputeFixations(DataDir,options):
    Files = os.listdir(DataDir)

    # Folder with a recording
    if "gaze2D.csv" in Files and "Gaze2DVideo.csv" in Files:
        CreateDirectory(os.path.join(DataDir,"FixationsLabel"))

    else:
        for f in Files:
            filepath = os.path.join(DataDir, f)
            if os.path.isdir(filepath):
                ProcessComputeFixations(filepath,options)


if __name__ == "__main__":

    (options, args) = GetParserFixations()
    if (options.input is None or not os.path.isdir(options.input)):
        print("ProcessGazeFixations: Wrong input file given in argument")
    else:
        ProcessComputeFixations(options.input,options)
