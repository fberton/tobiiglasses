"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import optparse
import os
from Sources.Tobii.SyncDataTobiiTraj import ProcessSynchonization
from Sources.Tobii.ExploreTobiiUnityData import ExploreProjectFolder,ExploreRecordings
from Sources.Tobii.GazeVideoAndEvents import ProcessRecordingVideoAndEvent
from Sources.General.FilesAndDirectory import CreateDirectory

def GetParser():
    parser = optparse.OptionParser("usage: %prog [options] arg1 arg2")
    parser.add_option("-i", "--intput", dest="input",
                      type="string", help="path to the user folder with all the data")
    parser.add_option("-o", "--output", dest="output",
                      type="string", help="path to the output folder (if not existing, it will create it)")
    return parser.parse_args()


def ProcessExtractTobbi(options):
    CreateDirectory(options.output)

    if os.path.isdir(options.input):
        ProjectDict = {}
        # give the name of the different projects and their ID
        ProjectDict = ExploreProjectFolder(options.input, options.output, ProjectDict)
        # Create the correct architecture of the project and move the data
        ExploreRecordings(options.input, options.output, ProjectDict)
        # Extract the data for each recording
        ProcessSynchonization(options.output)
        # Create the different folder for each event with video and the gaze
        ProcessRecordingVideoAndEvent(options.output)
    else:
        print("ExtractTobii: Input folder does not exists.")

if __name__ == "__main__":

    (options, args) = GetParser()
    ProcessExtractTobbi(options)

