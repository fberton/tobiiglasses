"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import optparse
import os
from Sources.ObjectsDetection.ObjectsDetection import ObjectsDetection
from Sources.ObjectsDetection.VideoObjectsDetection import ComputeDetection

def GetParserDetection():
    parser = optparse.OptionParser("usage: %prog [options] arg1 arg2")
    parser.add_option("-i", "--intput", dest="input",
                      type="string", help="path to the user folder with all the data (recursive search for gaze data)")
    parser.add_option("-m", "--methods", dest="methods",
                      choices=["Yolov3","tinyYolov3"],
                      default = "Yolov3",
                      help="methods used to detects objects in videos (Yolov3,tinyYolov3")

    return parser.parse_args()

# Recursive function to compute fixations in each folder
def ProcessVideoDetections(DataDir,Detector):
    Files = os.listdir(DataDir)

    # Folder with a recording
    if "Video.avi" in Files :
        ComputeDetection(DataDir,Detector)

    else:
        for f in Files:
            filepath = os.path.join(DataDir, f)
            if os.path.isdir(filepath):
                ProcessVideoDetections(filepath,Detector)


if __name__ == "__main__":

    (options, args) = GetParserDetection()
    Detectors = ObjectsDetection(options.methods)
    if (options.input is None or not os.path.isdir(options.input)):
        print("ProcessVideoDetection: Wrong input file given in argument")
    else:
        ProcessVideoDetections(options.input,Detector=Detectors)
