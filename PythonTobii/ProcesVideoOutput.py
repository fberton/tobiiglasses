"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import optparse
import os
from Sources.Videos.VideoGazeObjects import CreateVideoGaze
def GetParserVideoOutput():
    parser = optparse.OptionParser("usage: %prog [options] arg1 arg2")
    parser.add_option("-i", "--intput", dest="input",
                      type="string", help="path to the user folder with all the data (recursive search for gaze data)")
    parser.add_option("-d", "--detector", dest="detector",
                      choices=["Yolov3","tinyYolov3"],
                      default = None,
                      help="if wanted add objects detected in video with method: Yolov3,tinyYolov3")
    parser.add_option("-f", "--fixation", dest="fixation",
                      choices=["Centroid2D","Velocity2D"],
                      default=None,
                      help="if wanted add gaze fixation in video with method: Centroid2D,Velocity2D")

    return parser.parse_args()

# Recursive function to create video output in each folder
def ProcessVideoOutput(DataDir,Fixation=None,Detection=None):
    Files = os.listdir(DataDir)

    # Folder with a recording
    if "Video.avi" in Files :
        CreateVideoGaze(DataDir, Fixation, Detection)

    else:
        for f in Files:
            filepath = os.path.join(DataDir, f)
            if os.path.isdir(filepath):
                ProcessVideoOutput(filepath,Fixation,Detection)


if __name__ == "__main__":

    (options, args) = GetParserVideoOutput()
    if (options.input is None or not os.path.isdir(options.input)):
        print("ProcessVideoOutput: Wrong input file given in argument")
    else:
        ProcessVideoOutput(options.input,Fixation=options.fixation,Detection=options.detector)
