"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import numpy as np


# simple linear interpolation
def InterpolateData(tc,t1,v1,t2,v2):
    return v1 + (tc-t1)/(t2-t1)*(v2-v1)


def Vecnorm(X):
    NormX = np.sqrt(np.sum(X*X,1))
    return np.reshape(NormX,(NormX.shape[0],1))