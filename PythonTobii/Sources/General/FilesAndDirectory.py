"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import os
import numpy as np


def CreateDirectory(DirPath):
    if not os.path.isdir(DirPath):
        os.mkdir(DirPath)


# parser for csv file
def ParseCsvFile(filename):
    #open File
    F = open(filename, 'r')
    Lines = F.readlines()
    F.close()
    infoAll = [[float(x) for x in L.split(",")] for L in Lines if len(L.split(",")) > 0]
    infoAll = np.array(infoAll)

    return infoAll


# write numpy matrix in csv forms
def WriteCsvFile(DataFilename,data,delimiter=","):

    if len(data.shape)>2:
        data =np.reshape(data,data.shape[1:])
    F = open(DataFilename,"w")
    text = "\n".join([ delimiter.join([ str(x) for x in Rows]) for Rows in data.tolist()])
    F.write(text)
    F.close()