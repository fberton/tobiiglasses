"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import cv2
import os
import numpy as np


# draw the last fixation with a color
def DrawLastFixation(img,GazeCenter,LastFixation,CoefR=1,Color=[255,0,0]):
    rayon = 0.015 * img.shape[0]
    if LastFixation>=0:
        idx = np.where(GazeCenter[:,2]==LastFixation)[0]
        center = np.sum(GazeCenter[idx, :], axis=0) / len(idx)
        center = (int(center[0]),int(center[1]))
        cv2.circle(img, center, int(rayon*0.75),Color , thickness=int(2*CoefR), lineType=8, shift=0)
        for i in idx:
            g = (int(GazeCenter[i,0]),int(GazeCenter[i,1]))
            cv2.circle(img, g, 1, Color, thickness=int(7*CoefR), lineType=8, shift=0)
            cv2.line(img,g,center,Color, thickness=int(1*CoefR))

    return img

# draw the current fixation
def DrawCurrentFixation(img,GazeCenter,idC,CoefR=1,Color=[0,0,255]):
    rayon = 0.015 * img.shape[0]
    IdFixation = GazeCenter[idC,2]

    if IdFixation>=0:
        idx = np.where(GazeCenter[:,2]==IdFixation)[0]
        center = np.sum(GazeCenter[idx, :], axis=0) / len(idx)
        center = (int(center[0]),int(center[1]))
        cv2.circle(img, center, int(rayon*0.75),Color, thickness=int(2*CoefR), lineType=8, shift=0)
        for i in idx:
            if (i<idC):
                g = (int(GazeCenter[i,0]),int(GazeCenter[i,1]))
                cv2.circle(img, g, 1,Color, thickness=int(7*CoefR), lineType=8, shift=0)
                cv2.line(img,g,center,Color, thickness=int(1*CoefR))
        #draw line toward next fixation
        if (IdFixation-1>=0):
            idx = np.where(GazeCenter[:,2] == IdFixation-1)[0]
            center2 = np.sum(GazeCenter[idx, :], axis=0) / len(idx)
            center2 = (int(center2[0]), int(center2[1]))
            cv2.line(img, center2, center, [0, 0, 0], thickness=int(2*CoefR))

    return img

# draw gaze point when it is in a saccade
def DrawSaccades(img,GazeCenter,LastFixation,idC,CoefR=1,Color=[0,255,0]):
    rayon = 0.015 * img.shape[0]
    GazePoint = (int(GazeCenter[idC,0]),int(GazeCenter[idC,1]))
    cv2.circle(img, GazePoint, int(rayon), Color, thickness=int(2 * CoefR), lineType=8, shift=0)
    # draw line toward next fixation
    if (LastFixation>= 0):
        idx = np.where(GazeCenter[:, 2] == LastFixation)[0]
        center2 = np.sum(GazeCenter[idx, :], axis=0) / len(idx)
        center2 = (int(center2[0]), int(center2[1]))
        cv2.line(img, center2,GazePoint, [0, 0, 0], thickness=int(2 * CoefR))

    return img

# Draw simple gaze on video
def DrawGaze(img,gaze,color = [0, 0, 255]):

    height, width, depth = img.shape
    center = (int(gaze[0] * width), int(gaze[1] * height))
    rayon = 0.015 * width

    cv2.circle(img, center, int(rayon), color, thickness=4, lineType=8, shift=0)
    cv2.circle(img, center, int(rayon / 2), color, thickness=4, lineType=8, shift=0)

    return img
