"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import cv2
import os
import csv



def draw_label(image, text, color, coords):
    font = cv2.FONT_HERSHEY_PLAIN
    font_scale = 1.
    (text_width, text_height) = cv2.getTextSize(text, font, fontScale=font_scale, thickness=1)[0]

    padding = 5
    rect_height = text_height + padding * 2
    rect_width = text_width + padding * 2

    (x, y) = coords

    cv2.rectangle(image, (x, y), (x + rect_width, y - rect_height), color, cv2.FILLED)
    cv2.putText(image, text, (x + padding, y - text_height + padding), font,
                fontScale=font_scale,
                color=(255, 255, 255),
                lineType=cv2.LINE_AA)

    return image

def TableColor(nb):

    nbChanel = int(nb/3)
    step = int(190/nbChanel)
    ColorTables = [(x/255,y/255,z/255) for x in range(60,250,step) for y in range(60,250,step) for z in range(60,250,step)]
    F = open(os.path.join("..","ObjectsDetection","YOLOv3","data","coco.names"),"r")
    Lines = F.readlines()
    F.close()
    DictCol = {}
    for a,b in zip(Lines,ColorTables):
        DictCol[a.rstrip()] = b

    return DictCol

def DrawBoxesDetection(img,Boxes):
    DictCol = TableColor(82)

    for b in Boxes:
        bint = [int(float(x)) for x in b[0:5]]
        cv2.rectangle(img, (bint[0], bint[1]), (bint[0]+bint[2], bint[1]+bint[3]), DictCol[b[5]], 1, cv2.LINE_AA)
        text = '{0} {1:.2f}'.format(b[5], float(b[4]))
        img = draw_label(img, text, DictCol[b[5]], (bint[0], bint[1]))

    return img

if __name__ == "__main__":
    imgPath = os.path.join("..","ObjectsDetection","YOLOv3","data","ImageTest.png")
    BoxesFiles = os.path.join("..","ObjectsDetection","YOLOv3","data","tinyYolov3_Boxes.csv")
    csvfile = open(BoxesFiles, newline='')
    Boxes = list(csv.reader(csvfile))
    img = cv2.imread(imgPath)
    imgres = DrawBoxesDetection(img, Boxes)
    cv2.imshow("res",imgres)
    cv2.waitKey(0)