"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import cv2
import os
import numpy as np
from Sources.General.FilesAndDirectory import ParseCsvFile
from Sources.Videos.DrawGaze import *
from Sources.Videos.DrawObjectsDetections import *

# Main function for create the output video
def ProcessVideo(VideoFilename,GazeCenter,ObjectsDetected,CoefR = 1.0):
    if os.path.isfile(VideoFilename):
        ##Init Video Information
        Video = cv2.VideoCapture(VideoFilename)
        width = int(Video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(Video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fourcc = cv2.VideoWriter_fourcc(*'DIVX')  # cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')  # cv2.VideoWriter_fourcc(*'DIVX')
        framerate = Video.get(5)
        widthN = int(width * CoefR)
        heightN = int(height * CoefR)
        videoOut = cv2.VideoWriter(VideoFilename.replace(".avi","_Output.avi"), fourcc, framerate, (widthN, heightN))
        LastFixation = -1

        ## pass the gaze location in pixel
        GazeCenter[:,0:2] = GazeCenter[:,0:2]*np.array([width,height])

        ## Loop
        for i in range(GazeCenter.shape[0]):
            ret, img = Video.read()

            # Draw Gaze
            if GazeCenter.shape[1]==2:
                img = DrawGaze(img,GazeCenter[i,:])
            elif  GazeCenter.shape[1]==3:
                img = DrawLastFixation(img, GazeCenter, LastFixation)
                if GazeCenter[i,2] >= 0:
                    img = DrawCurrentFixation(img,GazeCenter,i)
                    LastFixation = GazeCenter[i,2]-1
                else:
                    img = DrawSaccades(img, GazeCenter, LastFixation, i)

            # Draw Objects Detected
            if ObjectsDetected is not None:
                if i in ObjectsDetected.keys():
                    img = DrawBoxesDetection(img, ObjectsDetected[i])

            # resize image
            resizeImg = cv2.resize(img, (widthN, heightN))
            videoOut.write(resizeImg)
    else:
        print("VideoGaze: video.avi is missing")

def AddGazeLabel(DataDir,GazeVideo,fixationMethods):

    if (os.path.isdir(os.path.join(DataDir,"FixationsLabel")) and fixationMethods is not None):
        PathgazeLabel = os.path.join(DataDir,"FixationsLabel",fixationMethods+".csv")
        if os.path.isfile(PathgazeLabel):
            GazeLabel = ParseCsvFile(PathgazeLabel)
            GazeLabelVideo = -1*np.ones((GazeVideo.shape[0],1))
            for i,info in enumerate(GazeVideo):
                # not a missing data
                if np.sum(np.abs(info[1:]))>0:
                    GazeLabelVideo[i] = GazeLabel[np.abs(GazeLabel[:,0]-GazeVideo[i,0]).argmin(),1]

            GazeVideo = np.concatenate((GazeVideo,GazeLabelVideo),axis=1)

    return GazeVideo[:,1:]

def GetBoxesDetection(DataDir,Detection):
    Boxes = None

    if Detection is not None:
        PathBoxes = os.path.join(DataDir, "ObjectsDetection",Detection+"_Boxes.csv")
        if os.path.isfile(PathBoxes):
            csvfile = open(PathBoxes, newline='')
            Data = list(csv.reader(csvfile))
            Boxes = {}
            for d in Data:
                frame = int(float(d[0]))
                boxe = d[2:8]
                if frame in Boxes.keys():
                    Boxes[frame].append(boxe)
                else:
                    Boxes[frame] = [boxe]


    return Boxes

def CreateVideoGaze(DataDir,Fixation=None,Detection=None):
    PathGaze = os.path.join(DataDir,"Gaze2DVideo.csv")

    if os.path.isfile(PathGaze):
        GazeVideo = ParseCsvFile(PathGaze)
        GazeVideo = AddGazeLabel(DataDir,GazeVideo,Fixation)
        ObjectsDetected = GetBoxesDetection(DataDir,Detection)
        ProcessVideo(os.path.join(DataDir,"Video.avi"), GazeVideo,ObjectsDetected,CoefR=1.0)



if __name__ == "__main__":
    DataDir = "..\\..\\Test\\TestTobiiController\\p1\\rec01_10_24_2019_14_53_25\\Seg_0\\Event_0"
    CreateVideoGaze(DataDir,Fixation="Centroid2D",Detection="Yolov3")