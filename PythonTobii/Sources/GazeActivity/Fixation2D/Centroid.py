"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import numpy as np
from Sources.General.Calcul import Vecnorm

def Centroid2D(gaze,ImageSize=[1920,1080],FovImage=90,ThDistDeg=1.5,DurationF=0.1,ThStdDeg=0.4):

    ## Init
    L = gaze.shape[0]
    RayonTh = ThDistDeg * (ImageSize[0] / FovImage)
    StdTh = ThStdDeg * (ImageSize[0] / FovImage)
    GazePix = gaze[:,1:] * ImageSize
    GazeLabel = -np.ones((L,1))
    dt = gaze[-1,0]/L
    WindowSize = np.round(DurationF/dt)

    ## Search fixations
    CenterDistStdId = np.zeros((L,5))
    for i in range(L):
        # test if lost data
        if (GazePix[i,0]==0 and GazePix[i,1]==0):
            CenterDistStdId[i,:] = [0,0,i,ImageSize[0]^2+ImageSize[1]^2,ImageSize[0]^2+ImageSize[1]^2]
        else:
            i1 = int(np.max([0,i-np.floor(WindowSize/2)]))
            i2 = int(min([L,i+np.floor(WindowSize/2)]))
            CenterN = np.sum(GazePix[i1:i2+1,:],0)/(i2-i1+1)
            distPoints = Vecnorm(GazePix[i1:i2+1,:]-CenterN)
            CenterDistStdId[i,0:2] = CenterN
            CenterDistStdId[i,2] = i
            CenterDistStdId[i,3] = np.max(distPoints)
            CenterDistStdId[i,4] = np.std(distPoints)


    ### Fixations fill
    FixWindow = np.zeros((L,1))
    ## take only points when all gaze are within a circle with a radius of degreeF
    InterestingFix = CenterDistStdId[np.where(CenterDistStdId[:,3]<RayonTh),:]
    InterestingFix = np.reshape(InterestingFix,InterestingFix.shape[1:])
    ## sort according of std of distance between gaze data and centroid
    InterestingFix = InterestingFix[InterestingFix[:,4].argsort(),:]
    Fixations = []
    
    ## Main loop
    for i in range(InterestingFix.shape[0]):
        Idcenter = InterestingFix[i,2]
        i1 = int(max([1,Idcenter-np.floor(WindowSize/2)]))
        i2 = int(min([L,Idcenter+np.floor(WindowSize/2)]))
        # check if we are not already on a fixation
        if np.sum(np.abs(FixWindow[i1:i2+1]))==0:
            FixWindow[i1:i2]=1
            # calcul of the new center
            CenterN = np.sum(GazePix[i1:i2+1,:],0)/(i2-i1+1)
            distPoints = Vecnorm(GazePix[i1:i2+1,:])-CenterN
            StdCenter = np.std(distPoints)
            Idx=[i1,i2]
            step = [-1,+1]
            TestFix= [True, True]
            stdPrec = StdCenter
            # search two way
            while(TestFix[0] or TestFix[1]):
                for j in [0,1]:
                    if TestFix[j]:
                        # get new points and values
                        Idx[j] = int(np.min([np.max([1,Idx[j]+step[j]]),L])) # new index
                        tmpCenterN = np.sum(GazePix[Idx[0]:Idx[1]+1,:],0)/(Idx[1]-Idx[0]+1) # new center
                        tmpDistPoint = Vecnorm(GazePix[Idx[0]:Idx[1]+1,:]-tmpCenterN)

                        # Conditions tests
                        TestStd = np.std(tmpDistPoint)-stdPrec<StdTh
                        TestRayon = np.mean(tmpDistPoint)<RayonTh

                        if ((TestStd and TestRayon) and FixWindow[Idx[j]]==0):
                            FixWindow[Idx[j]] = 1 # consider gaze points as fixation
                            TestFix[j] = True
                        else:
                            TestFix[j] = False
                            Idx[j] = Idx[j]-step[j]

            # fill if fixation size superior to windowSize
            if (Idx[1]-Idx[0]+1)>=WindowSize:
                Fixations.append([Idx[0],Idx[1]])

    ## Sorted fixations
    Fixations = np.array(Fixations)
    Fixations = Fixations[Fixations[:,0].argsort(),:] # sorted fixations

    ## give the correct label
    for i in range(Fixations.shape[0]):
        GazeLabel[Fixations[i,0]:Fixations[i,1]] = i

    GazeLabel = np.concatenate((gaze[:,[0]],GazeLabel),axis=1)
    return GazeLabel

