"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import optparse
import os
import numpy as np
from Sources.General.FilesAndDirectory import ParseCsvFile,WriteCsvFile
from Sources.GazeActivity.Fixation2D.Centroid import Centroid2D

def GetParserFixations():
    parser = optparse.OptionParser("usage: %prog [options] arg1 arg2")
    parser.add_option("-i", "--intput", dest="input",
                      type="string", help="path to the user folder with all the data (recursive search for gaze data)")
    parser.add_option("-m", "--methods", dest="methods",
                      choices=["Centroid2D","Velocity2D","AngleDistance3D"],
                      default = "Centroid2D",
                      help="methods used to compute fixations (Centroid2D,Velocity2D,AngleDistance3D")

    ### Fixations parameters
    parser.add_option("--ThDistDeg", dest="ThDistDeg", type=float, default=1.5) # works for all fixations
    parser.add_option("--DurationF", dest="DurationF", type=float, default=0.1)  # works for all fixations

    ## Fixations 2D
    # Image informations
    parser.add_option("--FovImage", dest="FovImage", type=int, default=90)
    parser.add_option("--ImageWidth", dest="ImageWidth", type=int, default=1920)
    parser.add_option("--ImageHeight", dest="ImageHeight", type=int, default=1080)

    # methods parameters
    parser.add_option("--ThStdDeg", dest="ThStdDeg", type=float, default=0.4) # Centroid method
    parser.add_option("--DilationSe", dest="DilationSe", type=int, default=3) # Velocity method
    parser.add_option("--RatioTh", dest="RatioTh", type=float, default=0.4) # Velocity method

    return parser.parse_args()

# Apply the correct methods to compute the gaze fixation
def ComputeFixations(dataDir,options):

    # check if fixation are computed in 2d (image space) or 3d
    if "3D" in options.methods:
        if options.EyeDir =="Right":
            gazeDir = ParseCsvFile(os.path.join(dataDir,"gazeRightEye.csv"))
            gazeNonDir = ParseCsvFile(os.path.join(dataDir, "gazeLeftEye.csv"))
        else:
            gazeDir = ParseCsvFile(os.path.join(dataDir, "gazeRightEye.csv"))
            gazeNonDir = ParseCsvFile(os.path.join(dataDir, "gazeLeftEye.csv"))

        gazeData = np.concatenate((gazeDir,gazeNonDir[1:,:]))
    else:
        gazeData = ParseCsvFile(os.path.join(dataDir,"gaze2D.csv"))

    # apply the correct methods
    if options.methods == "Centroid2D":
        GazeLabel = Centroid2D(gazeData,[options.ImageWidth,options.ImageHeight],options.FovImage,options.ThDistDeg,options.DurationF,options.ThStdDeg)
    else:
        print("ComputeFixations: Wrong methods to compute fixations")
        return

    # write data
    WriteCsvFile(os.path.join(DataDir, "FixationsLabel", options.methods + ".csv"), GazeLabel)

if __name__ == "__main__":
    DataDir = "..\\..\\Test\\TestTobiiController\\p1\\rec01_10_24_2019_14_53_25\\Seg_0\\Event_0"
    (options, args) = GetParserFixations()
    ComputeFixations(DataDir,options)

