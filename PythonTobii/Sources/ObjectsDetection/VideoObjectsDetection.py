"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import cv2
import os
import numpy as np
from Sources.ObjectsDetection.ObjectsDetection import ObjectsDetection
from Sources.General.FilesAndDirectory import CreateDirectory,WriteCsvFile


def DetectInVideo(videoFilename,Detector):
    ##Init
    Video = cv2.VideoCapture(videoFilename)
    success = True
    frame = 0
    # output format : frame, video time, bounding boxes, classes
    OutputBoxes = []

    ## Main Loop
    while success:
        success, image = Video.read()
        if success:
            TimeFrame = Video.get(0) / 1000.0
            Boxes =  Detector.ProcessDetection(image)
            for b in Boxes:
                OutputBoxes.append([frame,TimeFrame] + b)
            frame+=1

    return OutputBoxes
# Apply the correct methods to compute the gaze fixation
def ComputeDetection(dataDir,Detector=None):

    # create the output directory if not exist
    OutputDir = os.path.join(dataDir, "ObjectsDetection")
    CreateDirectory(OutputDir)

    # check if detector is not None
    if Detector is not None:
        videoFilename = os.path.join(dataDir, "Video.avi")
        # Get the Boxes
        Boxes = DetectInVideo(videoFilename, Detector)
        # write data
        WriteCsvFile(os.path.join(OutputDir,Detector.mode+"_Boxes.csv"),np.array(Boxes))

if __name__ == "__main__":
    DataDir = "..\\..\\Test\\TestTobiiController\\p1\\rec01_10_24_2019_14_53_25\\Seg_0\\Event_0"
    Detector = ObjectsDetection("tinyYolov3")
    ComputeDetection(DataDir,Detector)

