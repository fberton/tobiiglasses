import cv2
from tensorflow.keras import Input, Model
from Sources.ObjectsDetection.YOLOv3.darknet import DarknetLoader
from Sources.ObjectsDetection.YOLOv3.predict import preprocess_image,handle_predictions
from Sources.ObjectsDetection.YOLOv3.yolo_head import yolo_head
import os
import numpy as np

from Sources.General.FilesAndDirectory import WriteCsvFile
class ObjectsDetection:

    def __init__(self,mode):
        self.mode=mode
        self.InitMode()

    def InitMode(self):
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
        os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"
        
        if self.mode=="Yolov3":
            inputs = Input(shape=(None, None, 3))
            DarknetL = DarknetLoader('yolov3')
            outputs, self.config = DarknetL.darknet_base(inputs, include_yolo_head=False)
            self.model = Model(inputs, outputs)
        elif self.mode=="tinyYolov3":
            inputs = Input(shape=(None, None, 3))
            DarknetL = DarknetLoader('yolov3-tiny')
            outputs, self.config = DarknetL.darknet_base(inputs, include_yolo_head=False)
            self.model = Model(inputs, outputs)
        else:
            inputs = Input(shape=(None, None, 3))
            DarknetL = DarknetLoader('yolov3')
            outputs, self.config = DarknetL.darknet_base(inputs, include_yolo_head=False)
            self.model = Model(inputs, outputs)

    def YoloDetector(self,img):
        image, image_data = preprocess_image(img, model_image_size=(self.config['width'], self.config['height']))

        predictions = yolo_head(self.model.predict([image_data]), num_classes=80,
                                input_dims=(self.config['width'], self.config['height']))

        boxes, classes, scores = handle_predictions(predictions,
                                                    confidence=0.3,
                                                    iou_threshold=0.4)
        TrueBoxes = []
        if boxes is not None:
            height, width = image.shape[:2]
            ratio_x = width / self.config['width']
            ratio_y = height / self.config['height']

            for b,s,c in zip(boxes,scores,classes):
                x, y, w, h = b
                # Rescale box coordinates
                x1 = x * ratio_x
                y1 = y * ratio_y
                w1 = w * ratio_x
                h1 = h * ratio_y
                TrueBoxes.append([x1, y1, w1, h1,s,self.config['labels'][c]])

        return TrueBoxes

    def ProcessDetection(self,img):
        if self.mode == 0 or self.mode == 1:
            Detection = self.YoloDetector(img)
        else:
            Detection = self.YoloDetector(img)

        return Detection

    def Process(self,img):
        Detection = self.ProcessDetection(img)
        for r1 in Detection:
            cv2.rectangle(img, (int(r1[0]), int(r1[1])), (int(r1[0]+r1[2]), int(r1[1]+r1[3])),[255,150,135] , 1, cv2.LINE_AA)
        return img





if __name__ == "__main__":

    Detect = ObjectsDetection("tinyYolov3")
    img = cv2.imread(os.path.join("YoloV3","Data","ImageTest.png"))
    Boxes = Detect.ProcessDetection(img)
    WriteCsvFile(os.path.join("YoloV3","Data", Detect.mode + "_Boxes.csv"), np.array(Boxes))
    # cv2.imshow("Output Image",imgRes)
    # cv2.waitKey(0)