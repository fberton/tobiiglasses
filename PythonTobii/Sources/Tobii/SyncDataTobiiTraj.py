"""
Florian Berton (florian.berton@inria.fr)
Julien Bruneau (julien.bruneau@inria.fr)

This file is part of the OpmOps and BEAR project
"""
import json
import numpy as np
import math
import os
from Sources.General.FilesAndDirectory import WriteCsvFile

def CreateRotation(Pos):
    rot = []
    for i,p in enumerate(Pos):
        if (i<len(Pos)-1):
            x = Pos[i+1][0]-p[0]
            z = Pos[i + 1][2] - p[2]
            a = np.arctan2(x,z)*180/np.pi
            rot.append([0,a,0])
        else:
            rot.append(rot[-1])

    return np.matrix(rot)


def createRotMat(dirVect, angle):
    c=math.cos(angle)
    s=math.sin(angle)
    x=dirVect[0,0]
    y=dirVect[1,0]
    z=dirVect[2,0]
    rotMat=np.matrix([[x*x*(1-c)+c,\
                          x*y*(1-c)-z*s,\
                          x*z*(1-c)+y*s \
                          ], \
                         [x*y*(1-c)+z*s,\
                          y*y*(1-c)+c,\
                          y*z*(1-c)-x*s\
                          ], \
                         [x*z*(1-c)-y*s,\
                          y*y*(1-c)+x*s,\
                          z*z*(1-c)+c\
                          ]\
                         ])
    return rotMat


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def processTobiiGaze(dataDir):
    """ Process the tobbi data of the cave or real experiment contain in 'trialDir'
    Use data from the eye director in priority (eye=0 => Right, eye=1 => Left)
    Create the 3 csv file : player.csv, gaze.csv and target.csv
    """
    # ##################################################################
    #
    #                          READ GAZE DATA
    #
    # ##################################################################
    fRecInfo = open(os.path.join(dataDir,"livedata.json"), 'r')
    tobiiData = fRecInfo.readlines()
    fRecInfo.close()
    
    
    leftPC=np.array([])   # Pupil Center
    leftPD=np.array([])   # Pupil Diameter
    leftGD=np.array([])   # Gaze Direction
    rightPC=np.array([])  # Pupil Center
    rightPD=np.array([])  # Pupil Diameter
    rightGD=np.array([])  # Gaze Direction 
    GP=np.array([])       # 2d Gaze Position
    GP3=np.array([])      # 3d Gaze Position
    ETS1=np.array([])     # First Synchro Event Time
    ETS2=np.array([])     # Second Synchro Event Time
    VTS=np.array([])      # Video Synchro Time
    
    gyro=np.array([])     # Gyroscope
    accel=np.array([])    # Accelerometer
    Events = []           # Events during recording

    for s in tobiiData:
        data=json.loads(s)
        if 'pc' in data.keys():
            if data["eye"]=="left":
                leftPC = np.append(leftPC, np.array([data["ts"], data["gidx"], data["pc"][0], data["pc"][1], data["pc"][2]]))
            else:
                rightPC = np.append(rightPC, np.array([data["ts"], data["gidx"], data["pc"][0], data["pc"][1], data["pc"][2]]))
        elif 'pd' in data.keys():
            if data["eye"]=="left":
                leftPD = np.append(leftPD, np.array([data["ts"], data["gidx"], data["pd"]]))
            else:
                rightPD = np.append(rightPD, np.array([data["ts"], data["gidx"], data["pd"]]))
        elif 'gd' in data.keys():
            if data["eye"]=="left":
                leftGD = np.append(leftGD, np.array([data["ts"], data["gidx"], data["gd"][0], data["gd"][1], data["gd"][2]]))
            else:
                rightGD = np.append(rightGD, np.array([data["ts"], data["gidx"], data["gd"][0], data["gd"][1], data["gd"][2]]))
        elif 'gp' in data.keys():
            GP = np.append(GP, np.array([data["ts"], data["gidx"], data["gp"][0], data["gp"][1]]))
        elif 'gp3' in data.keys():
            GP3 = np.append(GP3, np.array([data["ts"], data["gidx"], data["gp3"][0], data["gp3"][1], data["gp3"][2]]))
        elif 'ets' in data.keys():
            tmpData=json.loads(data["tag"].replace("'",'"'))
            if 'unityTime' in tmpData.keys():
                Events.append([data["ts"],tmpData["msg"]])
                # ETS1 = np.append(ETS1, np.array([tmpData["idSync"],data["ts"],data["ets"],tmpData["unityTime"],tmpData["StreamTime"]]))
            #else:
            #ETS2 = np.append(ETS2, np.array([tmpData["idSync"],data["ts"],data["ets"],tmpData["onStreamReceived"],tmpData["StreamTime"]]))
        elif 'vts' in data.keys():
            VTS = np.append(VTS, np.array([data["ts"], data["vts"]]))
        elif 'gy' in data.keys():
            gyro = np.append(gyro, np.array([data["ts"], data["gy"][0], data["gy"][1], data["gy"][2]]))
        elif 'ac' in data.keys():
            accel = np.append(accel, np.array([data["ts"], data["ac"][0], data["ac"][1], data["ac"][2]]))
                
    leftPC = np.reshape(leftPC, (-1,5))
    rightPC = np.reshape(rightPC, (-1,5))
    
    leftPD = np.reshape(leftPD, (-1,3))
    rightPD = np.reshape(rightPD, (-1,3))
    
    leftGD = np.reshape(leftGD, (-1,5))
    rightGD = np.reshape(rightGD, (-1,5))
    
    GP = np.reshape(GP, (-1,4))
    GP3 = np.reshape(GP3, (-1,5))
    
    ETS1 = np.reshape(ETS1, (-1,5))
    ETS2 = np.reshape(ETS2, (-1,5))
    
    VTS = np.reshape(VTS, (-1,2))
    
    tmp=min(ETS1.shape[0],ETS2.shape[0])
    ETS1=ETS1[1:tmp,:]
    ETS2=ETS2[1:tmp,:]
    

    gyro = np.reshape(gyro, (-1,4))
    accel = np.reshape(accel, (-1,4))    
    
    # ##################################################################
    #
    #                          JOIN DATA BY GIDX
    #
    # ##################################################################   
    minGIDX = int(min(np.concatenate((rightPC[:,1],rightGD[:,1],leftPC[:,1],leftGD[:,1],GP3[:,1]))))
    maxGIDX = int(max(np.concatenate((rightPC[:,1],rightGD[:,1],leftPC[:,1],leftGD[:,1],GP3[:,1]))))    
    
    tobiiData=np.empty((maxGIDX-minGIDX+1,16))
    tobiiData[:]=np.nan
    
    idxRPC=0
    idxRGD=0
    idxLPC=0
    idxLGD=0
    idxGP3=0
    idx=0
    for x in range(minGIDX, maxGIDX+1):
        currEts=np.array([])
        #===============================================================
        while (idxRPC<np.shape(rightPC)[0]-1 and rightPC[idxRPC,1]<x):
            idxRPC=idxRPC+1
        
        if rightPC[idxRPC,1]!=x:
            print("ERROR : rightPC has not recording for GIDX = %d" % x)
        else:
            tobiiData[idx,1:4]=rightPC[idxRPC,2:5]/1000
            currEts=np.append(currEts,rightPC[idxRPC,0])
        #===============================================================
        while (idxRGD<np.shape(rightGD)[0]-1 and rightGD[idxRGD,1]<x):
            idxRGD=idxRGD+1
        
        if rightGD[idxRGD,1]!=x:
            print("ERROR : rightGD has not recording for GIDX = %d" % x)
        else:
            tobiiData[idx,4:7]=rightGD[idxRGD,2:5]
            currEts=np.append(currEts,rightGD[idxRGD,0])
        #===============================================================
        while (idxLPC<np.shape(leftPC)[0]-1 and leftPC[idxLPC,1]<x):
            idxLPC=idxLPC+1
        
        if leftPC[idxLPC,1]!=x:
            print("ERROR : leftPC has not recording for GIDX = %d" % x)
        else:
            tobiiData[idx,7:10]=leftPC[idxLPC,2:5]/1000
            currEts=np.append(currEts,leftPC[idxLPC,0])
        #===============================================================
        while (idxLGD<np.shape(leftGD)[0]-1 and leftGD[idxLGD,1]<x):
            idxLGD=idxLGD+1
        
        if leftGD[idxLGD,1]!=x:
            print("ERROR : leftGD has not recording for GIDX = %d" % x)
        else:
            tobiiData[idx,10:13]=leftGD[idxLGD,2:5]
            currEts=np.append(currEts,leftGD[idxLGD,0])
        #===============================================================
        while (idxGP3<np.shape(GP3)[0]-1 and GP3[idxGP3,1]<x):
            idxGP3=idxGP3+1
        
        if GP3[idxGP3,1]!=x:
            print("ERROR : GP3 has not recording for GIDX = %d" % x)
        else:
            tobiiData[idx,13:16]=GP3[idxGP3,2:5]/1000
            currEts=np.append(currEts,GP3[idxGP3,0])
        #===============================================================
        tobiiData[idx,0]=np.mean(currEts)        
        idx=idx+1    
            
        
    # ##################################################################
    #
    #                  LINK GAZE DATA WITH UNITY DATA
    #
    # ##################################################################
    # COMPUTE FUNCTION TO CONVERT GLASSES TIME TO UNITY TIME
    unityGaze= np.concatenate( (tobiiData[:,0:1], tobiiData[:,1:16]), axis=1)

    # Synchronize unity and tobii data
    gazeRightEye=np.array([])
    gazeLeftEye=np.array([])
    j=0
    while j<unityGaze.shape[0]:
        gazeRightEye = np.append(gazeRightEye, unityGaze[j,0:7])
        gazeLeftEye = np.append(gazeLeftEye, np.concatenate((unityGaze[j,0:1],unityGaze[j,7:13])) )
        j=j+1

    gazeRightEye =  np.reshape(gazeRightEye, (-1,7))
    gazeLeftEye =  np.reshape(gazeLeftEye, (-1,7))
        
        
    # ##################################################################
    #
    #                  LINK GAZE DATA WITH TOBII VIDEO
    #
    # ##################################################################
    ## COMPUTE FUNCTION TO CONVERT GLASSES TIME TO VIDEO TIME
    z = np.polyfit(VTS[:,0], VTS[:,1], 1)
    p = np.poly1d(z)
    videoGaze= np.concatenate( (p(GP[:,0:1])/1000000, GP[:,2:5]), axis=1)
    videoGyro= np.concatenate( (p(gyro[:,0:1])/1000000, gyro[:,1:4]), axis=1)
    videoAccel= np.concatenate( (p(accel[:,0:1])/1000000, accel[:,1:4]), axis=1)
    videoGazeRightEye = np.concatenate( (p(gazeRightEye[:,0:1])/1000000, gazeRightEye[:,1:]), axis=1)
    videoGazeLeftEye = np.concatenate((p(gazeLeftEye[:, 0:1]) / 1000000, gazeLeftEye[:, 1:]), axis=1)

    ## COMPUTE FUNCTION TO CONVERT GLASSES TIME TO UNITY TIME
    np.savetxt(os.path.join(dataDir,'gaze2D.csv'), videoGaze, '%10.4f', delimiter=',')
    np.savetxt(os.path.join(dataDir,'gyroscope.csv'), videoGyro, '%10.4f', delimiter=',')
    np.savetxt(os.path.join(dataDir,'accelerometer.csv'), videoAccel, '%10.4f', delimiter=',')
    np.savetxt(os.path.join(dataDir,'gazeRightEye.csv'), videoGazeRightEye, '%10.4f', delimiter=',')
    np.savetxt(os.path.join(dataDir,'gazeLeftEye.csv'), videoGazeLeftEye, '%10.4f', delimiter=',')

    ## Compute events
    ManageEvent(dataDir, Events, GP[-1,0],p)
    return
    
def ManageEvent(dataDir,Events,TimeMax,p):
    # check if there was some events
    if len(Events)>0:

        #check if we have an odd number of event and had a last
        if np.mod(len(Events),2):
            #check if the last one is a start
            if ("Start" in Events[-1][1]):
                Events.append([TimeMax,Events[-1][1].replace("Start","Stop")])
            else:
                print("SyncDataTobii: error in events number")
                return
        Text = ""
        NbEvent = [0,0]
        for i in range(int(len(Events)/2)):
            t1,e1 = Events[2*i]
            t2,e2 = Events[2*i+1]
            # check if it is a start stop event
            if "Start" in e1 and "Stop" in e2:
                # check if validation
                if "Valid" in e1 and "Valid" in e2:
                    eventName = "Validation_"+str(NbEvent[0])
                    NbEvent[0]+=1
                elif "Event" in e1 and "Event" in e2:
                    eventName = "Event_" + str(NbEvent[1])
                    NbEvent[1] += 1
                else:
                    print("SyncDataTobii: error in events name")
                    return
            else:
                print("SyncDataTobii: error in events number succession")
                return

            Text += eventName+","+str(p(t1)/1000000)+","+str(p(t2)/1000000)+"\n"

        #Write Events file
        F = open(os.path.join(dataDir,"Events.csv"),"w")
        F.write(Text)
        F.close()

def ProcessSynchonization(DataDir):
    Files = os.listdir(DataDir)

    # Folder with a recording
    if "RawDataTobii" in Files:
        processTobiiGaze(os.path.join(DataDir,"RawDataTobii"))
    else:
        for f in Files:
            filepath = os.path.join(DataDir, f)
            if os.path.isdir(filepath):
                ProcessSynchonization(filepath)

if __name__ == "__main__":
    DataDir = "..\\..\\Test\\TestTobiiController\\p1\\rec01_10_24_2019_14_53_25\\Seg_0\\RawData"
    processTobiiGaze(DataDir)
