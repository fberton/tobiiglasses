"""
Florian Berton (florian.berton@inria.fr)
Julien Bruneau (julien.bruneau@inria.fr)

This file is part of the OpmOps and BEAR project
"""
import numpy as np
import os
import cv2
import shutil
from Sources.General.Calcul import InterpolateData
from Sources.General.FilesAndDirectory import *

# interpolation for gaze in current frame
def InterpolateGazeVideo(tc,Gaze2D):
    DiffTime = Gaze2D[:,0] - tc
    idx = (np.abs(DiffTime)).argmin()

    # check if the time is a match otherwise perform an interpolation
    if (DiffTime[idx]==0):
        return Gaze2D[idx,1:]
    else:
        # get the two closest index time
        if DiffTime[idx]<0:
            t1 = idx
            t2 = np.min([idx+1,Gaze2D.shape[0]-1])
        else:
            t1 = np.max([idx - 1, 0])
            t2 = idx

        # check if no missing data
        if np.sum(np.abs(Gaze2D[t1,1:]))>0 and np.sum(np.abs(Gaze2D[t2,1:]))>0:
            return InterpolateData(tc,Gaze2D[t1,0],Gaze2D[t1,1:],Gaze2D[t2,0],Gaze2D[t2,1:])
        else:
            return Gaze2D[idx,1:]

# perform a linear interpolation when only or two data are missing
def PostProcessGazeData(Gaze):
    idx = 1
    while (idx<Gaze.shape[0]-2):
        #check if missing data
        if np.sum(np.abs(Gaze[idx,1:]))==0:
            if np.sum(np.abs(Gaze[idx-1,1:]))!=0 and np.sum(np.abs(Gaze[idx+1,1:]))!=0:
                Gaze[idx, 1:] = InterpolateData(Gaze[idx,0],Gaze[idx-1,0],Gaze[idx-1,1:],Gaze[idx+1,0],Gaze[idx+1,1:])
                idx+=1
            elif np.sum(np.abs(Gaze[idx-1,1:]))!=0 and np.sum(np.abs(Gaze[idx+2,1:]))!=0:
                Gaze[idx, 1:] = InterpolateData(Gaze[idx, 0], Gaze[idx - 1, 0], Gaze[idx-1, 1:], Gaze[idx + 2, 0],Gaze[idx + 2, 1:])
                Gaze[idx+1, 1:] = InterpolateData(Gaze[idx+1, 0], Gaze[idx - 1, 0], Gaze[idx-1, 1:], Gaze[idx + 2, 0],Gaze[idx + 2, 1:])
                idx+=2
    return Gaze

# class which contains all the data recorded by the tobii glasses
class TobiiData:

    def __init__(self,DataDir):
        self.Gaze2D = ParseCsvFile(os.path.join(DataDir,"gaze2D.csv"))
        self.GazeLeftEye = ParseCsvFile(os.path.join(DataDir, "gazeLeftEye.csv"))
        self.GazeRightEye = ParseCsvFile(os.path.join(DataDir, "gazeRightEye.csv"))
        self.Ac = ParseCsvFile(os.path.join(DataDir, "accelerometer.csv"))
        self.Gyro = ParseCsvFile(os.path.join(DataDir, "gyroscope.csv"))
        self.GetEvents(DataDir)

    # get time line of event
    def GetEvents(self,DataDir):
        pathEventFiles = os.path.join(DataDir,"Events.csv")
        if os.path.isfile(pathEventFiles):
            F = open(pathEventFiles,'r')
            self.Events = [L.rstrip().split(",") for L in F.readlines() if len(L.split(";"))>0]
            F.close()
            for i in range(len(self.Events)):
                for j in [1,2]:
                    self.Events[i][j] = float(self.Events[i][j])
        else:
            self.Events = [["Event_0",0,self.Gaze2D[-1,0]]]

    # Post Process Gaze value
    def PostProcessData(self):
        self.Gaze2D = PostProcessGazeData(self.Gaze2D)
        self.GazeLeftEye = PostProcessGazeData(self.GazeLeftEye)
        self.GazeRightEye = PostProcessGazeData(self.GazeRightEye)

# get sub-data with correct time
def GetSubDataTimeVideo(Data,t1,t2,TimeVideo):
    subData = Data[np.where((Data[:,0]>=t1) & (Data[:,0]<=t2)),:]
    if len(subData.shape)>2:
        subData = np.reshape(subData,subData.shape[1:])
    subData[:,0] = subData[:,0]- TimeVideo

    return subData

# create the directory for one recording event
def CreateRecordingEvent(eventR,DataDir,Data,TimeVideo):
    Folderpath = os.path.join(DataDir,eventR[0])
    if not os.path.isdir(Folderpath):
        os.mkdir(Folderpath)

    WriteCsvFile(os.path.join(DataDir,eventR[0],"gaze2D.csv"),GetSubDataTimeVideo(Data.Gaze2D, eventR[1], eventR[2], TimeVideo))
    WriteCsvFile(os.path.join(DataDir,eventR[0], "gazeLeftEye.csv"),GetSubDataTimeVideo(Data.GazeLeftEye, eventR[1], eventR[2], TimeVideo))
    WriteCsvFile(os.path.join(DataDir,eventR[0], "gazeRightEye.csv"),GetSubDataTimeVideo(Data.GazeRightEye, eventR[1], eventR[2], TimeVideo))
    WriteCsvFile(os.path.join(DataDir,eventR[0], "accelerometer.csv"),GetSubDataTimeVideo(Data.Ac, eventR[1], eventR[2], TimeVideo))
    WriteCsvFile(os.path.join(DataDir,eventR[0], "gyroscope.csv"),GetSubDataTimeVideo(Data.Gyro, eventR[1], eventR[2], TimeVideo))

    return Folderpath

# Create the video and video gaze data for each event
def ProcessVideo(DataDir,Data):
    ##Init
    # Video Information
    Video = cv2.VideoCapture(os.path.join(DataDir,"RawDataTobii","fullstream.mp4"))
    width = int(Video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(Video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fourcc = cv2.VideoWriter_fourcc(*'DIVX')  # cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')  # cv2.VideoWriter_fourcc(*'DIVX')
    framerate = Video.get(5)
    success = True
    # Event variables
    NewRecordingEvent = False # indicate if we are in a new recording event
    nb = 0
    gaze2DVideo = [] # gaze position in the current frame
    TimeVideoEvent = 0

    ## Main Loop
    while success and nb < len(Data.Events):

        success, image = Video.read()
        # test is we get the image
        if success:
            TimeFrame = Video.get(0) / 1000.0
            # test if the image is in the current window time
            if (TimeFrame >= Data.Events[nb][1] and TimeFrame <= Data.Events[nb][2]):
                # check if we are in a new event
                if not NewRecordingEvent:
                    EventDirectory = CreateRecordingEvent(Data.Events[nb], DataDir, Data,TimeFrame)
                    videoOut = cv2.VideoWriter(os.path.join(EventDirectory, "Video.avi"), fourcc, framerate,(width, height))
                    NewRecordingEvent = True

                videoOut.write(image)
                gaze2DVideo.append([TimeFrame-Data.Events[nb][1]]+InterpolateGazeVideo(TimeFrame,Data.Gaze2D).tolist()) # get the gaze location in the current frame
            else:
                # check if we have to release the video and the gaze points in the video
                if NewRecordingEvent:
                    videoOut.release()
                    WriteCsvFile(os.path.join(EventDirectory,"Gaze2DVideo.csv"), np.array(gaze2DVideo))
                    gaze2DVideo = []
                    NewRecordingEvent = False
                    nb += 1

    Video.release()
    return 0

# Recursive function to create the recording folder
def ProcessRecordingVideoAndEvent(DataDir):
    Files = os.listdir(DataDir)

    # Folder with a recording
    if "RawDataTobii" in Files:
        Data = TobiiData(os.path.join(DataDir, "RawDataTobii"))
        ProcessVideo(DataDir,Data)
        # remove the folder as it is not mandatory anymore
        shutil.rmtree(os.path.join(DataDir, "RawDataTobii"))
    else:
        for f in Files:
            filepath = os.path.join(DataDir, f)
            if os.path.isdir(filepath):
                ProcessRecordingVideoAndEvent(filepath)


if __name__ == "__main__":
    DataDir = "..\\..\\Test\\TestTobiiController\\p1\\rec01_10_24_2019_14_53_25\\Seg_0"
    ProcessRecordingVideoAndEvent(DataDir)