"""
Florian Berton (florian.berton@inria.fr)

This file is part of the OpmOps and BEAR project
"""

import re
import shutil
import gzip
import os
from Sources.General.FilesAndDirectory import CreateDirectory

def GetProjectNameAndId(inputFile):
    F = open(inputFile, "r")
    text = F.read()
    F.close()
    id = re.findall("pr_id\": \"(.*)\"", text)[0]
    project = re.findall("Name\": \"(.*)\"", text)[0]
    return (id,project)

def GetParticipantAndProjectId(inputFile):
    F = open(inputFile, "r")
    text = F.read()
    F.close()
    Participant = re.findall("Name\": \"(.*)\"", text)[0]
    project = re.findall("pa_project\": \"(.*)\"", text)[0]
    return (Participant,project)

def GetRecordingNameAndData(inputFile):
    F = open(inputFile, "r")
    text = F.read()
    F.close()
    Data = re.findall("CreationData\": \"(.*)\"", text)[0]
    TimeStr = "_"+Data.replace("/","_").replace(" ","_").replace(":","_")
    recording = re.findall("Name\": \"(.*)\"", text)[0]
    return recording+TimeStr

def ExploreProjectFolder(inputDir,outputDir,DictProjects):
    Files = os.listdir(inputDir)

    if "project.json" in Files:
        id,project = GetProjectNameAndId(os.path.join(inputDir,"project.json"))

        if id not in DictProjects.keys():
            DictProjects[id] = project
            if not os.path.isdir(os.path.join(outputDir,project)):
                os.mkdir(os.path.join(outputDir,project))
    else:
        for f in Files:
            filepath = os.path.join(inputDir,f)
            if os.path.isdir(filepath):
                DictProjects = ExploreProjectFolder(filepath,outputDir,DictProjects)

    return DictProjects

def ExtractRecording(InputRecPath,OutputRecPath):
    Segments = os.listdir(os.path.join(InputRecPath,"segments"))
    for i,s in enumerate(Segments):
        CreateDirectory(os.path.join(OutputRecPath,"Seg_"+str(i)))
        RawPath = os.path.join(OutputRecPath,"Seg_"+str(i),"RawDataTobii")
        CreateDirectory(RawPath)
        shutil.copyfile(os.path.join(InputRecPath,"segments",s,"fullstream.mp4"),os.path.join(OutputRecPath,"Seg_"+str(i),"RawDataTobii","fullstream.mp4"))
        f_in = gzip.open(os.path.join(InputRecPath,"segments",s,"livedata.json.gz"), 'rb')
        f_out = open(os.path.join(OutputRecPath,"Seg_"+str(i),"RawDataTobii","livedata.json"), 'wb')
        f_out.write(f_in.read())
        f_in.close()
        f_out.close()

def ExploreRecordings(inputDir,outputDir,DictProjects):
    Files = os.listdir(inputDir)

    # Folder with a recording
    if ("participant.json" in Files) and ("recording.json" in Files) and ("sysinfo.json" in Files):
        Participant, ProjectId = GetParticipantAndProjectId(os.path.join(inputDir,"participant.json"))
        Recording = GetRecordingNameAndData(os.path.join(inputDir, "recording.json"))
        CreateDirectory(os.path.join(outputDir,DictProjects[ProjectId],Participant))
        InputRecPath = os.path.join(outputDir, DictProjects[ProjectId], Participant,Recording)
        CreateDirectory(InputRecPath)
        ExtractRecording(inputDir, InputRecPath)

    else:
        for f in Files:
            filepath = os.path.join(inputDir,f)
            if os.path.isdir(filepath):
                ExploreRecordings(filepath,outputDir,DictProjects)

