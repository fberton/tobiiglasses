﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour {
    static string myLog ;
    private string output = "";
    private string stack  = "";
    public Text outputScreen;

    void OnEnable()
    {
        Application.RegisterLogCallback(HandleLog);
    }

    void OnDisable()
    {
        // Remove callback when object goes out of scope
        Application.RegisterLogCallback(null);
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        output = logString;
        stack = stackTrace;
        myLog += "\n" + output;
    }

    void OnGUI()
    {
        //myLog = GUI.TextArea(new Rect((float)10, (float)10, (float)Screen.width - (float)10, (float)Screen.height - (float)10), myLog);

        outputScreen.text = myLog;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
