﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JSonObject
{
    public string name;
    public string value;
    public List<JSonObject> children;

    public JSonObject(string n)
    {
        name = n;
        value = null;
        children = new List<JSonObject>();
    }

    public JSonObject(string n, string v)
    {
        name = n;
        value = v;
        children = new List<JSonObject>();
    }

    public JSonObject()
    {
        name = null;
        value = null;
        children = new List<JSonObject>();
    }

    public void addEntry(JSonObject entry)
    {
        children.Add(entry);
    }

    public string ToString(int space = 0)
    {
        string msg;
        msg = "";

        if (name!=null)
            msg =  name + " : ";

        if (value != null)
        {
            msg = msg + value;
            if (children.Count > 0)
                Debug.LogError("JSonObject has a value and children");
        }
        else
        {
            msg = msg + "[ \n";
            foreach (JSonObject jo in children)
                msg = msg + new string(' ', space+1) + jo.ToString(space+1) + " \n";
            msg = msg + new string(' ', space) + "]";
        }

        return msg;
    }

    public bool CheckEntry(string varName, string varValue)
    {
        if (name != null && name.Equals(varName))
            return varValue.Equals(value);
        else
        {
            bool res = false;
            foreach (JSonObject jo in children)
            {
                res = res || jo.CheckEntry(varName, varValue);
            }
            return res;
        }
    }

    public string LookForValue(string nameId)
    {
        JSonObject tmp = getSubJSon(nameId);
        if (tmp == null)
            return null;
        else
            return tmp.getValue();
    }

    public string getValue()
    {
        if (children.Count>0)
        {
            string data = "";
            foreach (JSonObject jo in children)
            {
                data =  data + jo.getValue() + ',';
            }
            data = data.Substring(0, data.Length-1);
            return data;
        }
        else 
            return value;
    }

    public JSonObject getSubJSon(string nameId)
    {
        JSonObject res = null;
        if (name != null && name.Equals(nameId))
            res = this;
        else
        {
            foreach (JSonObject jo in children)
            {
                res = jo.getSubJSon(nameId);
                if (res != null)
                    break;
            }

        }
        return res;
    }
}

public static class JSonParser {

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>
    // >>>> PARSE JSON STRING >>>>
    // >>>>>>>>>>>>>>>>>>>>>>>>>>>
    public static JSonObject PARSE(string jsonData, string objectName=null)
    {
        JSonObject result = new JSonObject(objectName);
        string parsingData = jsonData;
        parsingData=parsingData.TrimStart(' ');

        if (parsingData.Length <= 0)
            return result;

        switch (parsingData[0])
        {
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // >>>> PARSING ARRAY & OBJECT >>>>
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            case '[':
            case '{':
                {
                    parsingData = parsingData.Substring(1);
                    bool inString = false;
                    int nested = 0;
                    int curId = 0;
                    int start = 0;
                    int length = 0;

                    foreach (char c in parsingData)
                    {
                        length++;
                        if (inString)
                        {
                            if (c == '"')
                                inString = false;
                        }
                        else
                        {
                            switch (c)
                            {
                                case '"':
                                    {
                                        inString = true;
                                        break;
                                    }
                                case '[':
                                case '{':
                                    {
                                        nested++;
                                        break;
                                    }
                                case ']':
                                case '}':
                                    {
                                        nested--;
                                        break;
                                    }
                                case ',':
                                    {
                                        if (nested == 0)
                                        {
                                            result.addEntry(JSonParser.PARSE(parsingData.Substring(start, length - 1)));
                                            start = curId + 1;
                                            length = 0;
                                        }
                                        break;
                                    }
                            }
                            if (nested<0)
                            {
                                result.addEntry(JSonParser.PARSE(parsingData.Substring(start, length - 1)));
                                start = curId + 1;
                                length = 0;
                                break;
                            }
                        }
                        curId++;
                    }
                    break;
                }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // >>>> PARSING STRING or STRING : VALUE >>>>
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            case '"':
                {
                    parsingData = parsingData.Substring(1);
                    int id = parsingData.IndexOf('"');
                    string text = parsingData.Substring(0, id);
                    parsingData=parsingData.Substring(id + 1);
                    parsingData=parsingData.TrimStart(' ');
                    if (parsingData.Length>0 && parsingData[0]==':')
                    {
                        result=JSonParser.PARSE(parsingData.Substring(1),text);
                    }
                    else
                    {
                        result.value = text;
                    }

                    break;
                }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            // >>>> PARSING NONE STRING VALUE >>>>
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            default:
                {
                    result.value = parsingData;
                    break;
                }
        }

        return result;
    }
    // <<<<<<<<<<<<<<<<<<<<<<<<<<<
    // <<<< PARSE JSON STRING <<<<
    // <<<<<<<<<<<<<<<<<<<<<<<<<<<
}


