﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

/// <summary>
/// Config parameters (XML serializable)
/// </summary>
public class Config
{
    [XmlElement("Local_Interface")]
    public string lInterface;

    [XmlElement("Glasses_IP")]
    public string gIp;

    [XmlElement("Discovery_Port")]
    public string discPort;

    [XmlElement("Data_Port")]
    public string dataPort;

    [XmlElement("Project_ID")]
    public string projectID;

    [XmlElement("Participant_ID")]
    public string partID;

    [XmlElement("Recording_ID")]
    public string recID;

}