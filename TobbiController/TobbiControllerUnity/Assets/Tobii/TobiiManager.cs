﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.IO;
using System;
using UnityEngine;
using UnityEngine.UI;

public class TobiiManager : MonoBehaviour
{
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- VARIABLES                                                                                                                                                                     ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    private System.Diagnostics.Stopwatch watch;
    private const int warningRemainingTime = 15 * 60;


    public GameObject gazeRef;

    // ---------
    // REPORTING
    #region reporting
    private float _lastDownTime;
    private string reportStatus = "";
    private string reportStream = "";
    private string reportMain = "";
    #endregion
    // ---------

    // -----
    // STATE
    #region state
    private bool isStreaming;
    private bool isConnected;
    private bool isCalibrated;
    private bool isRecording;
    #endregion
    // -----

    // ---
    // GUI
    #region gui
    private Text tMain;
    private Text tStatus;
    private Text tStreaming;
    private Canvas cUI;
    #endregion
    // ---

    // ---------------------------
    // GUI buttons object
    #region buttons
    private Button bConnect;
    private Button bNew;
    private Button bStream;
    private Button bCalibrate;
    private Button bStartRec;
    private Button bValidation;
    private Button bEvent;
    #endregion


    // ---------------------------
    // GUI buttons booleans
    #region buttons
    private bool StartValidation = true;
    private bool StartEvent = true;
    #endregion

    // ---------------------------
    // Glasses communication infos
    #region stream
    private IPAddress interfaceIP6 = IPAddress.Parse("fe80::8c3a:5558:db25:1436");
    private IPAddress glassesIP6 = IPAddress.Parse("fe80::76fe:48ff:fe25:2368");
    private int discoverPort = 13006;
    private int dataPort = 49152;
    private int latence = 0;

    private byte[] MSG_DISCOVER = Encoding.ASCII.GetBytes("{\"type\":\"discover\"}");
    private byte[] MSG_KA_DATA = Encoding.ASCII.GetBytes("{\"type\": \"live.data.unicast\", \"key\": \"some_GUID\", \"op\": \"start\"}");
    private byte[] buffer;

    private Socket client;
    #endregion
    // ---------------------------

    // -------------------------
    // Glasses http request info
    #region httpRequest
    private string base_url = "http://[fe80::76fe:48ff:fe25:2368]";

    private string projectName;
    private string participantName;
    private string recordingName;

    private string IDproject;
    private string IDparticipants;
    private string IDcalibrations;
    private string IDrecording;

    private bool bSyncing;
    private int idSync;
    private string syncOnStream;
    private float lastSync;
    private float SyncPeriod;
    #endregion
    // -------------------------

    // ----
    // DATA
    #region data
    private float lastDataReceivedTime;
    private float[] left_pc;
    private float left_pd;
    private float[] left_gd;
    private float[] right_pc;
    private float right_pd;
    private float[] right_gd;
    private float[] gp;
    private float[] gp3;
    private float[] gy;
    private float[] ac;
    private int ts;
    private string caState;
    private string caErrors;
    #endregion
    // ----

    string getConfigPath() {

        string pathPlayer = Application.dataPath;
        int lastIndex = pathPlayer.LastIndexOf('/');
        return pathPlayer.Remove(lastIndex, pathPlayer.Length - lastIndex);
    }

    public void saveConfig()
    {
        Config conf=new Config();

        conf.lInterface = GameObject.Find("guiInterface").GetComponent<InputField>().text;
        conf.gIp = GameObject.Find("guiIP").GetComponent<InputField>().text;
        conf.discPort = GameObject.Find("guiDiscoPort").GetComponent<InputField>().text;
        conf.dataPort = GameObject.Find("guiDataPort").GetComponent<InputField>().text;
        conf.projectID = GameObject.Find("guiProjectId").GetComponent<InputField>().text;
        conf.partID = GameObject.Find("guiParticipantId").GetComponent<InputField>().text;
        conf.recID = GameObject.Find("guiRecordingId").GetComponent<InputField>().text;

        LoaderXML.CreateXML<Config>(getConfigPath() + @"/Config.xml", conf);
    }

    public void loadConfig()
    {
        string path = getConfigPath() + @"/Config.xml";
        if (File.Exists(path))
        {
            Config conf = (Config)LoaderXML.LoadXML<Config>(path);
            GameObject.Find("guiInterface").GetComponent<InputField>().text= conf.lInterface;
            GameObject.Find("guiIP").GetComponent<InputField>().text = conf.gIp;
            GameObject.Find("guiDiscoPort").GetComponent<InputField>().text = conf.discPort;
            GameObject.Find("guiDataPort").GetComponent<InputField>().text = conf.dataPort;
            GameObject.Find("guiProjectId").GetComponent<InputField>().text = conf.projectID;
            GameObject.Find("guiParticipantId").GetComponent<InputField>().text = conf.partID;
            GameObject.Find("guiRecordingId").GetComponent<InputField>().text = conf.recID;
        }
    }


    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- START                                                                                                                                                                         ----------
    //---------- Use for initialization                                                                                                                                                        ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region start
    void Start()
    {
        // ---------
        // REPORTING
        #region reporting
        _lastDownTime = 0;
        #endregion
        // ---------

        // -----
        // STATE
        #region state
        isStreaming = false;
        isConnected = false;
        isCalibrated = false;
        isRecording = false;
        #endregion
        // -----

        // ---
        // GUI
        #region gui
        tMain = GameObject.Find("tTobiiMain").GetComponent<Text>();
        tStatus = GameObject.Find("tTobiiStatus").GetComponent<Text>();
        tStreaming = GameObject.Find("tTobiiStream").GetComponent<Text>();
        cUI = GameObject.Find("StartingMenu").GetComponent<Canvas>();
        #endregion
        // ---

        // ---------------------------
        // GUI buttons object
        #region buttons
        bConnect = GameObject.Find("Connect").GetComponent<Button>();
        bNew = GameObject.Find("New").GetComponent<Button>();
        bStream = GameObject.Find("Stream").GetComponent<Button>();
        bCalibrate = GameObject.Find("Calibrate").GetComponent<Button>();
        bStartRec = GameObject.Find("StartRec").GetComponent<Button>();
        bValidation = GameObject.Find("Validation").GetComponent<Button>();
        bEvent = GameObject.Find("Event").GetComponent<Button>();
        #endregion

        // ---------------------------
        // Glasses communication infos
        #region stream
        buffer = new byte[256];
        #endregion
        // ---------------------------

        // -------------------------
        // Glasses http request info
        #region httpRequest    
        projectName = "";
        participantName = "";
        recordingName = "";

        IDproject = null;
        IDparticipants = null;
        IDcalibrations = null;
        IDrecording = null;

        bSyncing = false;
        idSync = 0;
        syncOnStream = "-1";
        lastSync = 0f;
        SyncPeriod = -1f;
        #endregion
        // -------------------------


        // ----
        // DATA
        #region data
        lastDataReceivedTime = -60 * 1000;
        left_pc = new float[3];
        left_pd = 0;
        left_gd = new float[3];
        right_pc = new float[3];
        right_pd = 0;
        right_gd = new float[3];
        gp = new float[2];
        gp3 = new float[3];
        gy = new float[3];
        ac = new float[3];
        ts = 0;
        caState = null;
        caErrors = null;
        #endregion
        // ----


        loadConfig();
        watch = new System.Diagnostics.Stopwatch();
        watch.Start();
    }
    #endregion

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- UPDATE                                                                                                                                                                        ----------
    //---------- Update info on GUI                                                                                                                                                            ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region update
    void Update()
    {
        if (!cUI.gameObject.activeSelf)
            return;

        reportMain = "";
        reportStream = "";
        reportStatus = "";

        updateGazeInUnity(new Vector3(gp3[0], gp3[1], gp3[2]));
        if (!isStreaming && !isConnected)// || watch.ElapsedMilliseconds - lastDataReceivedTime > 30 * 1000)
        {
            string HostName = Dns.GetHostName();
            reportMain = "Host Name of machine = " + HostName + "\n";
            IPAddress[] ipaddress = Dns.GetHostAddresses(HostName);
            reportMain = reportMain + "IP Address of Machine is" + "\n";
            foreach (IPAddress ip in ipaddress)
            {
                reportMain = reportMain + ip.ToString() + "\n";
            }


            bNew.interactable = false;
            bCalibrate.interactable = false;
            bStartRec.interactable = false;
            bValidation.interactable = true;
            bEvent.interactable = false;
        }
        else
        {
            bNew.interactable = true;
            bCalibrate.interactable = true;
            bStartRec.interactable = true;

            if (isConnected)
            {
                string tmp_reportStatus = "";
                if (IDproject != null)
                    tmp_reportStatus = tmp_reportStatus + "Project selected : " + projectName + "(" + IDproject + ")\n";
                else
                    tmp_reportStatus = tmp_reportStatus + "Project not selected\n";

                if (IDparticipants != null)
                    tmp_reportStatus = tmp_reportStatus + "Participant selected : " + participantName + "(" + IDparticipants + ")\n";
                else
                    tmp_reportStatus = tmp_reportStatus + "Participant not selected\n";

                if (IDcalibrations != null)
                {
                    tmp_reportStatus = tmp_reportStatus + "Calibration state : " + caState + "(" + IDcalibrations + ")\n";
                    tmp_reportStatus = tmp_reportStatus + caErrors + "\n";
                }
                else
                {
                    tmp_reportStatus = tmp_reportStatus + "No calibration\n";
                    tmp_reportStatus = tmp_reportStatus + "\n";
                }
                tmp_reportStatus = tmp_reportStatus + "----------------------------------------\n";
                tmp_reportStatus = tmp_reportStatus + getStatus();

                reportStatus = tmp_reportStatus;
            }
            if (isStreaming) {
                string temp_reportStream = "";
                temp_reportStream = temp_reportStream + "<b>pupil diameter</b>\n" + left_pd.ToString() + "-" + right_pd.ToString() + "\n";
                temp_reportStream = temp_reportStream + "<b>pupil center (L R)</b>\n";
                temp_reportStream = temp_reportStream + left_pc[0].ToString() + " " + left_pc[1].ToString() + " " + left_pc[2].ToString() + "\n";
                temp_reportStream = temp_reportStream + right_pc[0].ToString() + " " + right_pc[1].ToString() + " " + right_pc[2].ToString() + "\n";
                temp_reportStream = temp_reportStream + "<b>gaze direction (L R)</b>\n";
                temp_reportStream = temp_reportStream + left_gd[0].ToString() + " " + left_gd[1].ToString() + " " + left_gd[2].ToString() + "\n";
                temp_reportStream = temp_reportStream + right_gd[0].ToString() + " " + right_gd[1].ToString() + " " + right_pc[2].ToString() + "\n";
                temp_reportStream = temp_reportStream + "\n";
                temp_reportStream = temp_reportStream + "<b>Gyroscope</b>\n";
                temp_reportStream = temp_reportStream + gy[0].ToString() + " " + gy[1].ToString() + " " + gy[2].ToString() + "\n";
                temp_reportStream = temp_reportStream + "<b>Accelerometer</b>\n";
                temp_reportStream = temp_reportStream + ac[0].ToString() + " " + ac[1].ToString() + " " + ac[2].ToString() + "\n";
                temp_reportStream = temp_reportStream + "\n";
                temp_reportStream = temp_reportStream + "<b>GTS " + ts.ToString() + "</b>\n";
                temp_reportStream = temp_reportStream + "<b>UTS " + ((int)(Time.realtimeSinceStartup * 1000000)).ToString() + "</b>\n";

                reportStream = temp_reportStream;
            }
            if (isRecording)
            {
                bStartRec.GetComponentInChildren<Text>().text = "Stop Rec";
                bValidation.interactable = true;
                bEvent.interactable = true;

                if (SyncPeriod > 0 && watch.ElapsedMilliseconds - lastSync > SyncPeriod) {
                    timeSync();
                    lastSync = watch.ElapsedMilliseconds;
                }
            } else
            {
                bStartRec.GetComponentInChildren<Text>().text = "Start Rec";

                bValidation.interactable = false;
                bEvent.interactable = false;
            }
        }

        tMain.text = reportMain;
        //tStatus.text = reportStatus.value;
        //tStreaming.text = reportStream.value;

        tStatus.text = reportStatus;
        tStreaming.text = reportStream;
    }
    #endregion

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- LATE UPDATE                                                                                                                                                                   ----------
    //---------- Check keyboard action                                                                                                                                                         ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region lateupdate
    public void LateUpdate()
    {
        //if (bXPstarted)
        //    return;

        //float currentTime = (float)(watch.Elapsed.TotalSeconds); // in ms


        ///* If Down Arrow => Go next button */
        //if (Input.GetKey(KeyCode.DownArrow) && (currentTime - _lastDownTime) > 0.5f)
        //{
        //    _lastDownTime = currentTime;

        //    if (bStartXP.activeSelf == true)
        //        bStartXP.GetComponent<Button>().onClick.Invoke();
        //    else if (bCalibrate.activeSelf == true)
        //        bCalibrate.GetComponent<Button>().onClick.Invoke();
        //    else
        //        bConnect.GetComponent<Button>().onClick.Invoke();
        //}
        ///* If Down Arrow => Go next button */
        //if (Input.GetKey(KeyCode.UpArrow) && (currentTime - _lastDownTime) > 0.5f)
        //{
        //    _lastDownTime = currentTime;

        //    if (bCalibrate.activeSelf == true)
        //        bCalibrate.GetComponent<Button>().onClick.Invoke();
        //}
    }
    #endregion

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- ON APPLICATION QUIT                                                                                                                                                           ----------
    //---------- Close stream and other connection when exiting                                                                                                                                ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region onapplicationquit
    public void OnApplicationQuit()
    {
        //stopRecording();
        // end of application
        if (client != null)
            client.Close();
    }
    #endregion

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- OUTSIDE CONTROL                                                                                                                                                               ----------
    //---------- Allow control from third class                                                                                                                                                ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region outsideControl
    public void startAutomatedSync(float period = 500)
    {
        SyncPeriod = period;
    }

    public void stopAutomatedSync()
    {
        SyncPeriod = -1;
    }
    #endregion

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- DATA STREAMING                                                                                                                                                                ----------
    //---------- Stream with the tobbi                                                                                                                                                         ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region datastreaming
    // ---------------------------------
    // Start data streaming with glasses
    private void startStreaming()
    {

        interfaceIP6 = IPAddress.Parse(GameObject.Find("guiInterface").GetComponent<InputField>().text);
        glassesIP6 = IPAddress.Parse(GameObject.Find("guiIP").GetComponent<InputField>().text);
        discoverPort = int.Parse(GameObject.Find("guiDiscoPort").GetComponent<InputField>().text);
        dataPort = int.Parse(GameObject.Find("guiDataPort").GetComponent<InputField>().text);
        base_url = "http://[" + glassesIP6.ToString() + "]";

        // --------------------
        // START DATA STREAMING
        EndPoint received_end_point = new IPEndPoint(IPAddress.IPv6Any, 0);
        client = new Socket(AddressFamily.InterNetworkV6, SocketType.Dgram, ProtocolType.Udp);
        client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
        client.Bind(new IPEndPoint(interfaceIP6, dataPort));

        client.SendTo(MSG_KA_DATA, new IPEndPoint(glassesIP6, dataPort)); // Send data request
        client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 1000); // TIMEOUT

        int msgLength = 0;
        try
        {
            msgLength = client.ReceiveFrom(buffer, ref received_end_point);
        }
        catch (SocketException e)
        {
            if (e.ErrorCode != 0x274c)
                throw new Exception("Unexpected Socket Error: " + e.Message);
            else
                Debug.LogError("No answer from discovery socket (" + e.Message + ")");
        }

        if (msgLength > 0) // Msg received, start streaming
        {
            client.SendTo(MSG_KA_DATA, new IPEndPoint(glassesIP6, dataPort));
            client.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref received_end_point, receiveData, client);
            isStreaming = true;
            lastDataReceivedTime = watch.ElapsedMilliseconds;
        }
        else // Timeout, glasses not found
        {
            client.Close();
        }
        // --------------------
    }
    // Start data streaming with glasses
    // ---------------------------------

    // -----------------------------
    // Data streaming async function
    private void receiveData(IAsyncResult iar)
    {

        lastDataReceivedTime = watch.ElapsedMilliseconds;
        // ------------
        // GET THE DATA
        Socket recvSock = (Socket)iar.AsyncState;
        EndPoint clientEP = new IPEndPoint(IPAddress.Any, 0);
        int msgLen = recvSock.EndReceiveFrom(iar, ref clientEP);

        string tmp = Encoding.ASCII.GetString(buffer, 0, msgLen);
        JSonObject dataList = JSonParser.PARSE(tmp);

        // Parse data
        foreach (JSonObject d in dataList.children)
        {
            if (d.name.Equals("ts"))
            {
                ts = int.Parse(d.getValue());
            }
            else if (d.name.Equals("pc"))
            {
                string[] t = d.getValue().Split(',');
                float[] f = { float.Parse(t[0]), float.Parse(t[1]), float.Parse(t[2]) };

                if (dataList.LookForValue("eye").Equals("right"))
                    Array.Copy(f, right_pc, 3);
                else
                    Array.Copy(f, left_pc, 3);
            }
            else if (d.name.Equals("pd"))
            {
                if (dataList.LookForValue("eye").Equals("right"))
                    right_pd = float.Parse(d.value);
                else
                    left_pd = float.Parse(d.value);
            }
            else if (d.name.Equals("gd"))
            {
                string[] t = d.getValue().Split(',');
                float[] f = { float.Parse(t[0]), float.Parse(t[1]), float.Parse(t[2]) };

                if (dataList.LookForValue("eye").Equals("right"))
                    Array.Copy(f, right_gd, 3);
                else
                    Array.Copy(f, left_gd, 3);
            }
            else if (d.name.Equals("gp"))
            {
                string[] t = d.getValue().Split(',');
                gp[0] = float.Parse(t[0]);
                gp[1] = float.Parse(t[1]);
            }
            else if (d.name.Equals("gp3"))
            {
                string[] t = d.getValue().Split(',');
                gp3[0] = float.Parse(t[0]);
                gp3[1] = float.Parse(t[1]);
                gp3[2] = float.Parse(t[2]);
            }
            else if (d.name.Equals("gy"))
            {
                string[] t = d.getValue().Split(',');
                gy[0] = float.Parse(t[0]);
                gy[1] = float.Parse(t[1]);
                gy[2] = float.Parse(t[2]);
            }
            else if (d.name.Equals("ac"))
            {
                string[] t = d.getValue().Split(',');
                ac[0] = float.Parse(t[0]);
                ac[1] = float.Parse(t[1]);
                ac[2] = float.Parse(t[2]);
            }
            else if (d.name.Equals("ets"))
            {
                JSonObject subdataList = JSonParser.PARSE(dataList.LookForValue("tag").Replace('\'', '"'));
                if (subdataList.LookForValue("unityTime") != null)
                    syncOnStream = (watch.ElapsedMilliseconds * 1000).ToString();
            }
            //else if (d.name.Equals("marker2d"))
            //{
            //    Debug.Log(tmp);
            //}
        }

        // ------------

        if (isStreaming)
        {
            // ----------------------
            // RELAUNCH THE STREAMING
            client.SendTo(MSG_KA_DATA, new IPEndPoint(glassesIP6, dataPort));
            client.BeginReceiveFrom(buffer, 0, buffer.Length, SocketFlags.None, ref clientEP, receiveData, client);
            // ----------------------
        }
        else
        {
            recvSock.Close();
            client = null;
        }
    }
    // Data streaming async function
    // -----------------------------

    private void updateGazeInUnity(Vector3 gp)
    {
        //Vector3 gazePos = mainCam.transform.position +
        //                 (mainCam.transform.forward * gp.z +
        //                  mainCam.transform.up      * gp.y +
        //                 -mainCam.transform.right   * gp.x )
        //                 / 1000;


        //vrValue b = new vrVec3(gazePos.x, gazePos.y, gazePos.z);
        //MiddleVR.VRKernel.ExecuteCommand("CU_updateGazeRefPos", b);
    }
    #endregion

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- GLASSES REQUEST                                                                                                                                                               ----------
    //---------- HTML request to the glasses for config and creating recording                                                                                                                 ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region glassesRequest
    // -----------------------------------
    // Select glasses project or create it
    private void createProject()
    {
        projectName = GameObject.Find("guiProjectId").GetComponent<InputField>().text;

        // Check existing projects
        string answer = get_request("/api/projects");
        bool needCreate = true;
        JSonObject lData = JSonParser.PARSE(answer);

        foreach (JSonObject jo in lData.children)
        {
            if (jo.CheckEntry("Name", projectName))
            {
                needCreate = false;
                IDproject = jo.LookForValue("pr_id");
                break;
            }
        }

        // if it does not exist, create it
        if (needCreate)
        {
            string data = "{ " +
                "\"pr_info\" : { " +
                "\"CreationData\": \"" + DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss") + "\"," +
                "\"EagleId\": \"" + Guid.NewGuid() + "\"," +
                "\"Name\":\"" + projectName + "\" } }";

            answer = post_request("/api/projects", data);
            lData = JSonParser.PARSE(answer);
            IDproject = lData.LookForValue("pr_id");
        }
    }
    // Select glasses project or create it
    // -----------------------------------

    // ---------------------------------------
    // Select glasses participant or create it
    private void createParticipant()
    {
        participantName = GameObject.Find("guiParticipantId").GetComponent<InputField>().text;

        // Check existing participants
        string answer = get_request("/api/participants");
        bool needCreate = true;
        JSonObject lData = JSonParser.PARSE(answer);

        foreach (JSonObject jo in lData.children)
        {
            if (jo.CheckEntry("Name", participantName) && jo.CheckEntry("pa_project", IDproject))
            {
                needCreate = false;
                IDparticipants = jo.LookForValue("pa_id");
                break;
            }
        }

        // if it does not exist
        if (needCreate)
        {
            string data = "{ \"pa_project\" : \"" + IDproject + "\" ," +
                "\"pa_info\" : { " +
                "\"CreationData\": \"" + DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss") + "\"," +
                "\"EagleId\": \"" + Guid.NewGuid() + "\"," +
                "\"Name\":\"" + participantName + "\" } }";

            answer = post_request("/api/participants", data);

            lData = JSonParser.PARSE(answer);
            IDparticipants = lData.LookForValue("pa_id");
        }
    }
    // Select glasses participant or create it
    // ---------------------------------------

    // -------------------------
    // Create new calibration ID
    private void createCalibration()
    {
        string answer;
        JSonObject lData;

        string data = "{ \"ca_project\" : \"" + IDproject + "\" , " +
                "\"ca_type\" : \"default\" , " +
                "\"ca_participant\" : \"" + IDparticipants + "\" }";
        //Debug.Log(data);

        answer = post_request("/api/calibrations", data);

        lData = JSonParser.PARSE(answer);
        IDcalibrations = lData.LookForValue("ca_id");
    }
    // Create new calibration ID
    // -------------------------

    // -----------------------------------
    // Request recording time from glasses
    private TimeSpan getRecordingTime()
    {
        string answer = get_request("/api/recordings/" + IDrecording + "/status");
        JSonObject dataList = JSonParser.PARSE(answer);

        TimeSpan time = TimeSpan.FromSeconds(int.Parse(dataList.LookForValue("rec_length")));

        return time;
    }
    // Request recording time from glasses
    // -----------------------------------

    // ------------------
    // Get glasses status
    private string getStatus()
    {
        string status = "";
        string answer = get_request("/api/system/status");
        JSonObject dataList = JSonParser.PARSE(answer);
        JSonObject tmp;
        int seconds;
        TimeSpan time;

        // Glasses connection status
        tmp = dataList.getSubJSon("sys_headunit");
        status = status + "Head unit status : " + tmp.LookForValue("state") + "\n";

        // Battery remaining time
        tmp = dataList.getSubJSon("sys_battery");
        seconds = int.Parse(tmp.LookForValue("remaining_time"));
        time = TimeSpan.FromSeconds(seconds);
        if (seconds < warningRemainingTime)
            status = status + "<color=#ff0000ff><b>Battery " + time.ToString() + " left</b></color>\n";
        else
            status = status + "Battery " + time.ToString() + " left\n";


        // SD card remaining time
        tmp = dataList.getSubJSon("sys_storage");
        seconds = int.Parse(tmp.LookForValue("remaining_time"));
        time = TimeSpan.FromSeconds(seconds);
        if (seconds < warningRemainingTime)
            status = status + "<color=#ff0000ff><b>Storage " + time.ToString() + " left</b></color>\n";
        else
            status = status + "Storage " + time.ToString() + " left\n";

        // Is it recording
        tmp = dataList.getSubJSon("sys_recording");
        string state = tmp.LookForValue("rec_state");
        if (state != null && !state.Equals("failed") && !state.Equals("stale") && !state.Equals("done") && !state.Equals("stopped"))
        {
            IDrecording = tmp.LookForValue("rec_id");
            isRecording = true;
            status = status + "<color=#ff0000ff><b>" + state + " : " + getRecordingTime().ToString() + "</b></color>\n";
        } else
        {
            isRecording = false;
        }

        return status;
    }
    // Get glasses status
    // ------------------

    // ---------------
    // Start recording
    public void startRecording()
    {
        name = GameObject.Find("guiRecordingId").GetComponent<InputField>().text;

        string answer;
        JSonObject lData;

        if (!isRecording)
        {
            // -------------------
            // Create recording ID
            string data = "{ \"rec_participant\" : \"" + IDparticipants + "\", " +
                "\"rec_info\" : { " +
                "\"CreationData\": \"" + DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss") + "\"," +
                "\"EagleId\": \"" + Guid.NewGuid() + "\"," +
                "\"Name\":\"" + name + "\" } }";
            //Debug.Log(data);

            answer = post_request("/api/recordings", data);
            lData = JSonParser.PARSE(answer);
            IDrecording = lData.LookForValue("rec_id");
            // Create recording ID
            // -------------------

            // ---------------
            // Start recording
            if (IDrecording != null)
            {
                answer = post_request("/api/recordings/" + IDrecording + "/start");
                isRecording = true;
                //Debug.Log(answer);
            }
            // Start recording
            // ---------------
        }
    }
    // Start recording
    // ---------------

    // --------------
    // Stop recording
    public void stopRecording()
    {
        if (isRecording)
        {
            string answer = post_request("/api/recordings/" + IDrecording + "/stop");
            isRecording = false;
            IDrecording = null;
            //Debug.Log(answer);
        }
    }
    // Stop recording
    // --------------

    // ----------
    // Send event
    public void sendEvent(string msg = "")
    {
        Debug.Log("Send event");
        int unityTime = ((int)(watch.ElapsedMilliseconds));// convert time in seconds to time in microseconds 1=1000000
        string data = "{ \"ets\" : " + ((int)(watch.ElapsedMilliseconds)).ToString() + ", \"type\":\"ManualEvent\", " +
            "\"tag\": \"{" +
            " 'unityTime' : " + unityTime.ToString() + "," +
            " 'LastLatence' : " + latence.ToString() + "," +
            " 'msg' : '" + msg + "'" +
            "}\"}";
        string answer = post_request("/api/events", data);
        latence = ((int)(watch.ElapsedMilliseconds)) - unityTime;
        //Debug.Log("Event answered " + watch.ElapsedMilliseconds.ToString());
    }

    public void sendEvent(string time, string msg = "")
    {
        string data = "{ \"ets\" : " + time + ", \"type\":\"ManualEvent\", \"tag\":\"" + msg + "\"} ";
        string answer = post_request("/api/events", data);
        //Debug.Log(answer);
    }
    // Send event
    // ----------

    // -----------
    // Send marker
    public void sendMarker(Vector3 worldPos)
    {
        //string data = "{ \"ts\" : " + ts + ", " +
        //       "\"marker2d\": [" + videoPos.x + ", " + videoPos.y + "]," +
        //       "\"marker3d\": [" + worldPos.x + ", " + worldPos.y + ", " + worldPos.z + "]," +
        //       "\"s\": 0 }";
        //Debug.Log(data);
        //string requestLine = "/api/calibrations/" + IDcalibrations + "/marker/" + playerPos.x + "/" + playerPos.y + "/" + playerPos.z;

        string requestLine = "/api/calibrations/" + IDcalibrations + "/marker/" + worldPos.x + "/" + worldPos.y + "/" + worldPos.z;
        Debug.Log(requestLine);
        string answer = post_request(requestLine);
        Debug.Log(answer);
    }
    // Send marker
    // -----------
    #endregion

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- SYNC EVENT                                                                                                                                                                    ----------
    //---------- Send/Receive time synchronization event                                                                                                                                       ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region SyncEvent

    struct syncRequest
    {
        public HttpWebRequest r;
        public string d;
    }

    public void timeSync()
    {
        while (bSyncing) { }

        bSyncing = true;
        syncOnStream = "-1";
        idSync++;
        //Debug.Log("Send event " + watch.ElapsedMilliseconds.ToString());
        int unityTime = ((int)(watch.ElapsedMilliseconds));// convert time in seconds to time in microseconds 1=1000000
        string postData =
            "{ \"ets\" : " + (watch.ElapsedMilliseconds * 1000).ToString() + ", \"type\":\"ManualEvent\", " +
            "\"tag\": \"{" +
            " 'idSync' : " + idSync.ToString() + "," +
            " 'unityTime' : " + unityTime.ToString() + "," +
            " 'StreamTime' : " + ts.ToString() +
            "}\"}";



        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(base_url + "/api/events");
        byte[] data = Encoding.ASCII.GetBytes(postData);

        request.Method = "POST";
        request.ContentType = "application/json";
        request.ContentLength = data.Length;

        syncRequest sr = new syncRequest();
        sr.r = request;
        sr.d = postData;
        try
        {
            request.BeginGetRequestStream(new AsyncCallback(timeSyncRequestCallback), sr);
        }
        catch (WebException e)
        {
            bSyncing = false;
            Debug.LogError(e);
        }

    }

    private void timeSyncRequestCallback(IAsyncResult iar)
    {
        syncRequest sr = (syncRequest)iar.AsyncState;

        // End the operation
        Stream postStream = sr.r.EndGetRequestStream(iar);

        byte[] data = Encoding.ASCII.GetBytes(sr.d);
        postStream.Write(data, 0, data.Length);
        postStream.Close();

        try
        {
            // Start the asynchronous operation to get the response
            sr.r.BeginGetResponse(new AsyncCallback(timeSyncResponseCallback), sr.r);
        }
        catch (WebException e)
        {
            bSyncing = false;
            Debug.LogError(e);
        }
    }

    // -----------------------------
    // Data streaming async function
    private void timeSyncResponseCallback(IAsyncResult iar)
    {
        HttpWebRequest request = (HttpWebRequest)iar.AsyncState;

        // End the operation
        HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(iar);
        response.Close();


        string data = "{ \"ets\" : " + (watch.ElapsedMilliseconds * 1000).ToString() + ", \"type\":\"ManualEvent\", " +
           "\"tag\": \"{" +
            " 'idSync' : " + idSync.ToString() + "," +
            " 'onStreamReceived' : " + syncOnStream + "," +
            " 'StreamTime' : " + ts.ToString() +
           "}\"}";
        string answer = post_request("/api/events", data);
        bSyncing = false;
    }
    #endregion

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- BUTTONS                                                                                                                                                                       ----------
    //---------- Process buttons clicking                                                                                                                                                      ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region buttons
    public void sendButtonValidation()
    {
        if (StartValidation)
        {
            bValidation.GetComponentInChildren<Text>().text = "Stop Valid";
            bValidation.GetComponentInChildren<Text>().color = new Color(1f, 0f, 0f);
            sendEvent("StartValid");

        } else
        {
            bValidation.GetComponentInChildren<Text>().text = "Start Valid";
            bValidation.GetComponentInChildren<Text>().color = new Color(0f, 0f, 0f);
            sendEvent("StopValid");
        }
        StartValidation = !StartValidation;
    }

    public void sendButtonEvent()
    {
        if (StartEvent)
        {
            bEvent.GetComponentInChildren<Text>().text = "Stop Event";
            bEvent.GetComponentInChildren<Text>().color = new Color(1f, 0f, 0f);
            sendEvent("StartEvent");

        }
        else
        {
            bEvent.GetComponentInChildren<Text>().text = "Start Event";
            bEvent.GetComponentInChildren<Text>().color = new Color(0f, 0f, 0f);
            sendEvent("StopEvent");
        }
        StartEvent = !StartEvent;
    }

    public void toogleInterface()
    {
        if (cUI.gameObject.activeSelf)
            cUI.gameObject.SetActive(false);
        else
            cUI.gameObject.SetActive(true);
    }

    public void toogleRecording()
    {
        if (isRecording)
            stopRecording();
        else
            startRecording();
    }

    public void latencyTest()
    {
        if (!isRecording)
            startRecording();

        timeSync();
        timeSync();
        timeSync();
        timeSync();
        timeSync();
        stopRecording();
    }

    public void exit()
    {
        Application.Quit();
    }

    public void toggleStreaming()
    {
        if (isStreaming)
            isStreaming = false;
        else
            startStreaming();
    }

    public void connectToGlasses()
    {
        if (isConnected)
            isConnected = false;
        else
        {
            try
            {
                getStatus();
                isConnected = true;
            }
            catch (SocketException e)
            {
                Debug.LogError("Connection failed (" + e.Message + ")");
            }
        }
    }

    public void SetupGlasses() {
        createProject();
        if (IDproject != null)
            createParticipant();
    }

    public void calibrate()
    {
        if (IDproject == null || IDparticipants==null)
            return;

        createCalibration();
        // -----------------------------------------
        // Start calibration and wait for the result
        if (IDcalibrations != null)
        {
            string answer;
            JSonObject lData;


            //mainCam = Camera.main.gameObject;

            //Vector3 playerPos = Camera.main.gameObject.transform.position;

            //Debug.Log("Cam pos = " + playerPos.ToString());

            //GameObject calibPosObj = GameObject.Find("CalibRealPos");
            //Vector3 calibPos = calibPosObj.transform.position;
            //Debug.Log("calibPos = " + calibPos.ToString());

            //Vector3 toObject = (calibPos - playerPos);
            //Debug.Log("toObject = " + toObject.ToString());
            //calibPos = new Vector3(-Vector3.Dot(toObject, mainCam.transform.right), Vector3.Dot(toObject, mainCam.transform.up), Vector3.Dot(toObject, mainCam.transform.forward));

            //Debug.Log("In cam system = " + calibPos.ToString());

            answer = post_request("/api/calibrations/" + IDcalibrations + "/start");
            //Debug.Log(answer);

            string calStatus = get_request("/api/calibrations/" + IDcalibrations + "/status");
            lData = JSonParser.PARSE(calStatus);
            while (lData.LookForValue("ca_state").Equals("calibrating"))
            {
                //if (XMLConfigLoader.useMiddleVR)
                //    sendMarker(calibPos*100);
                calStatus = get_request("/api/calibrations/" + IDcalibrations + "/status");
                lData = JSonParser.PARSE(calStatus);
            }

            caState=lData.LookForValue("ca_state");
            caErrors = lData.LookForValue("ca_error");

            if (caState.Equals("calibrated"))
            {
                //bShowStart.value = true;
                isCalibrated = true;
            }
                
        }
        // Start calibration and wait for the result
        // -----------------------------------------
    }
    #endregion

    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //---------- POST_REQUEST                                                                                                                                                                  ----------
    //---------- Send/Receive html request                                                                                                                                                     ----------
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    #region htmlRequest
    private string post_request(string apiAction, string postData = "")
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(base_url + apiAction);
        byte[] data = Encoding.ASCII.GetBytes(postData);

        request.Method = "POST";
        request.ContentType = "application/json";
        request.ContentLength = data.Length;

        try
        {
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
        catch (WebException e)
        {
            Debug.LogError(e);
            return "";
        }
    }

    private string get_request(string apiAction)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(base_url + apiAction);

        request.Method = "GET";
        request.ContentType = "application/json";

        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        return new StreamReader(response.GetResponseStream()).ReadToEnd();

    }
    #endregion

}
